Categories:Multimedia
License:Apache-2.0
Web Site:https://simplemobiletools.github.io/
Source Code:https://github.com/SimpleMobileTools/Simple-Contacts
Issue Tracker:https://github.com/SimpleMobileTools/Simple-Contacts/issues
Changelog:https://github.com/SimpleMobileTools/Simple-Contacts/blob/HEAD/CHANGELOG.md

Auto Name:Contacts
Summary:A contacts app for managing your contacts without ads
Description:
A simple app for creating or managing your contacts from any source. The
contacts can be stored on your device only, but also synchronized via Google, or
other accounts. You can display your favorite contacts on a separate list.

You can use it for managing user emails and events too. It has the ability to
sort/filter by multiple parameters, optionally display surname as the first
name.

Contains no ads or unnecessary permissions. It is fully opensource, provides
customizable colors.

This app is just one piece of a bigger series of apps. You can find the rest of
them at http://www.simplemobiletools.com
.

Repo Type:git
Repo:https://github.com/SimpleMobileTools/Simple-Contacts

Build:3.0.1,2
    commit=3.0.1
    subdir=app
    gradle=yes

Build:3.0.3,4
    commit=3.0.3
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:3.0.3
Current Version Code:4
